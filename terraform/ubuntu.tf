data "aws_ami" "ubuntu" {
  most_recent = true

  filter = {
    name   = "name"
    values = ["ubuntu/images/hvm-io1/ubuntu-trusty-14.04-amd64-server*"]
  }

  filter = {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter = {
    name   = "root-device-type"
    values = ["ebs"]
  }

  owners = ["099720109477"]
}

resource "aws_security_group" "ubuntu" {
  name        = "ubuntu_security_group"
  description = "Allow HTTP and SSH"
  vpc_id      = "${aws_vpc.fuse.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["52.7.76.128/32", "107.212.240.224/32"] # Fuse's and my source IPs
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["52.7.76.128/32", "107.212.240.224/32"] # Fuse's and my source IPs
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["52.7.76.128/32", "107.212.240.224/32"] # Fuse's and my source IPs
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "ubuntu" {
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "t2.micro"
  subnet_id              = "${aws_subnet.fuse.id}"
  vpc_security_group_ids = ["${aws_security_group.ubuntu.id}"]
  key_name               = "${var.ssh_keypair_name}"

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = "${file("./fuse.pem")}"
  }

  provisioner "file" {
    source      = "./motd"
    destination = "/home/ubuntu/motd"
  }

  provisioner "file" {
    source      = "./provision.sh"
    destination = "/home/ubuntu/provision.sh"
  }

  provisioner "file" {
    source      = "../chef"
    destination = "/tmp/chef"
  }

  provisioner "file" {
    source      = "../docker"
    destination = "/tmp/docker"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo bash /home/ubuntu/provision.sh",
    ]
  }
}

output "ip" {
  value = "${aws_instance.ubuntu.public_ip}"
}
