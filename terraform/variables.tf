variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "ssh_keypair_name" {
  default = "fuse"
}
