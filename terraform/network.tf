provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "us-east-2"
}

resource "aws_vpc" "fuse" {
  cidr_block           = "192.168.0.0/16"
  instance_tenancy     = "default"
  enable_dns_hostnames = true
}

resource "aws_subnet" "fuse" {
  vpc_id                  = "${aws_vpc.fuse.id}"
  cidr_block              = "192.168.0.0/24"
  map_public_ip_on_launch = true
}

resource "aws_internet_gateway" "fuse" {
  vpc_id = "${aws_vpc.fuse.id}"
}

resource "aws_route_table" "fuse" {
  vpc_id = "${aws_vpc.fuse.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.fuse.id}"
  }
}

resource "aws_route_table_association" "fuse" {
  route_table_id = "${aws_route_table.fuse.id}"
  subnet_id      = "${aws_subnet.fuse.id}"
}
