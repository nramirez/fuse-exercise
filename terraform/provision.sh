# Create user with home directory
fuse_password=$(openssl passwd -1 "fuse")
useradd --create-home --gid sudo --password $fuse_password fuse

# Allow SSH with password
sed 's/PasswordAuthentication no/PasswordAuthentication yes/' -i /etc/ssh/sshd_config

# Restart SSH daemon
service ssh restart

# Install ChefDK
wget https://packages.chef.io/files/stable/chefdk/3.1.0/ubuntu/14.04/chefdk_3.1.0-1_amd64.deb -O /tmp/chefdk.deb
dpkg -i /tmp/chefdk.deb

# Create the Chef logs folder
mkdir /tmp/chef/logs

# Run Chef
cd /tmp/chef
chef-solo --config solo.rb -j nodes/ubuntu.json

# Install Docker
if [ ! $(which docker) ]; then
    echo "----Installing docker----"
    sudo apt update
    sudo apt install -y ifupdown apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt update
    sudo apt install -y docker-ce
fi

# Run the Docker container
cd /tmp/docker
docker build -t fusenginx:latest .
docker run -p 8080:80 -d fusenginx

# Copy MOTD file
cp /home/ubuntu/motd /etc/motd