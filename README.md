# Fuse Exercise

Author: Nick Ramirez

## About the exercise

Everything is created with `terraform apply`.

* Uses Terraform to create a VM in AWS
* Chef installs Apache and runs it on port 80
* Installs Docker and runs NGINX in a container
* NGINX container redirects port 8080 to port 80
* Chef log is exposed through link on Apache webpage

SSH login:

Username: `fuse`
Password: `fuse`

Folders on VM:

* /tmp/chef
* /tmp/docker
* /etc/apache2