# fuse-apache

This cookbook does the following:

* Installs Apache
* Creates a default HTML page named fuse.html
* Configures Apache to load fuse.html as the default page

