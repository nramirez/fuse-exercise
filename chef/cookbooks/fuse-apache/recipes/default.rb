#
# Cookbook:: fuse-apache
# Recipe:: default
#
# Copyright:: 2018, Nick Ramirez, All Rights Reserved.

package "apache2"

template "/var/www/html/fuse.html" do
  source "fuse.html.erb"
  owner "root"
  group "root"
  variables(
      hostname: node['hostname'],
      ip_address: node['ipaddress'],
      chef_log_path: node['chef_log_path']
  )
end

template "/etc/apache2/sites-enabled/000-default.conf" do
  source "000-default.conf.erb"
  owner "root"
  group "root"
  notifies :restart, 'service[apache2]', :immediately
end

service "apache2" do
  action :nothing
end